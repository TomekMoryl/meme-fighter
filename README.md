# MEME FIGHTER

- Game made for "A Company", simple rpg game with exploration, fighting and leveling your character. Game is written in 
Java 8.

![World Map](screenshots/map.png)

- You need to use keyboard and written commands in this game, all of the commands you can use will be displayed on the
screen, on the world map they will be on the right side, during combat and interaction on the bottom.

#### Requirements

- To run this game you need [Java 8,](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html "Oracle page")

- I'm recommending of using [Intellij IDE](https://www.jetbrains.com/idea/ "JetBrains") If you want to run from IDE.

## Running from terminal

- This is recommended way for playing this game because screen will be cleared with every move.

##### What to use Linux/Windows

- In Linux (ubuntu) default terminal is great choice because it will handle all ASCII images

- In Windows Powershell is fine choice but personally i recommend using cmd (Start -> type 'cmd' and press enter), second
better choice is using [ConEmu](https://conemu.github.io/ "ConEmu")

##### Running Jar

- Jar File will be provided with this project in folder **jar**, you can find **meme-fighter.jar**

- With terminal of your choice go to **meme-fighter\jar** folder and type **java -jar meme-fighter.jar** after that game will start. Saves
will be written to the directory where is jar file

- You may need to expand your terminal windows to fit all the elements.

![Windows](screenshots/jar-windows.png)

## Run with docker (**WARNING** save don't work in docker)

- You can also run game using [Docker](https://www.jetbrains.com/idea/ "Docker") by typing in your CLI
`docker run -ti ramalash/meme_fighter`, and you will be able to play game, but you are not able to save game progress.

![DOCKER](screenshots/docker.png)

## Running from IDE (Intellij)

##### Clone repository

- Clone repository on your computer. Open root folder (meme-fighter) in Intellij, you should see something like this

![MAIN](screenshots/Main.png)

- If your icons will look differently then open **Main** and you should be able too see at top this toolbar click on
**Setup SDK**

![SDK SETUP](screenshots/sdk-setup.png)

- You should be able to see your java that is installed on your pc if not click **Configure...**

![JAVA SDK](screenshots/java-sdk.png)

- In configure you need to find green **+** sign and click on it 

![ADD SDK](screenshots/add-sdk.png)

- Find JDK on your pc and select it 

![FIND SDK](screenshots/find-jdk.png)

- Now you have JDK to build this project, but if you still can't see blue icons. You may need to select **source** 
for your project. click on the **meme-fighter** folder on the left toolbar **Project** and press F4, click on **src**
and mark it as **source**. Accept changes and you can run game.

![SOURCE](screenshots/source.png)

##### Run game from Intellij

- You can run game from Intellij terminal, from the left toolbar **Project** find file **Main** click with right mouse
button and select **Run 'Main.main()'** you will be able to play from Intellij terminal but it is not recommended way
to play this game.

![RUN](screenshots/intellij-run.png)

- Intellij terminal won't clear game screen with new content, but you are able to finish it.

![PROBLEM](screenshots/problem.png)

- In this approach the screen won't be cleared after moves.