package pl.tomekMoryl.fileHandling;

import java.io.*;

public class ReadFile {

    public String fileReader(String fileName) {
        String str;
        StringBuilder buf = new StringBuilder();
        try (InputStream is = this.getClass().getResourceAsStream(fileName)) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            while ((str = reader.readLine()) != null) {
                buf.append(str).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buf.toString();
    }
}
