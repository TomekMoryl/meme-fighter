package pl.tomekMoryl.gameLogic;

import pl.tomekMoryl.constants.DisplayConstants;
import pl.tomekMoryl.constants.MessageColors;
import pl.tomekMoryl.gameMap.WorldMap;
import pl.tomekMoryl.gameMap.logic.MapMovement;
import pl.tomekMoryl.player.Player;
import pl.tomekMoryl.player.PlayerLogic;
import pl.tomekMoryl.player.PlayerPosition;

import java.util.Scanner;

public class Game {
    private WorldMap worldMap = new WorldMap();
    private MapMovement mapMovement = new MapMovement();
    public static boolean gameEnd = false;
    private String moveMessages = "";
    private PlayerPosition playerPosition = new PlayerPosition(1, 2);

    public void startGame() {
        Player player = gameMenu();

        while (!gameEnd) {
            DisplayConstants.clearScreen();
            worldMap.printMap(playerPosition);
            System.out.println(moveMessages);
            moveMessages = mapMovement.makeMove(worldMap, playerPosition, player);
        }

    }

    private Player gameMenu() {
        System.out.println(DisplayConstants.menuLogo);
        boolean isOptionSelected = false;
        while (!isOptionSelected) {
            System.out.println("Type option you want to chose");
            System.out.println(MessageColors.RED + "(1) New Game\n" + MessageColors.MAGENTA + "(2) Load Game");
            Scanner scanner = new Scanner(System.in);
            switch (scanner.nextLine()) {
                case ("1"): {
                    Player player = PlayerLogic.newGame();
                    DisplayConstants.gameStart();
                    return player;
                }
                case ("2"): {
                    Player player = PlayerLogic.loadGame(worldMap, playerPosition);
                    if (!player.getName().equals("")) return player;
                }
                default: {
                    System.out.println("Somethings wrong use other option");
                    DisplayConstants.waitForEnter();
                }

            }
        }
        return null;
    }
}
