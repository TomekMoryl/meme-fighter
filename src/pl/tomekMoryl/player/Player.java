package pl.tomekMoryl.player;

import java.io.Serializable;
import java.util.Objects;

public class Player extends PlayerPosition implements Serializable {

    private static final long serialVersionUID = 4572498L;

    private String name = "";
    private int experience = 0;
    private int level = 1;
    private int HP = 100;
    private String heroClass = "";
    private int attackPower = 1;
    private int skillsPower = 1;
    private int defense = 1;
    private Items userItems = new Items();
    private String equippedWeapon = "";
    private boolean isTutorialFinished = false;

    public Player(int x, int y) {
        super(x, y);
    }

    public Player(int x, int y, String name, int experience, int level, int HP, String heroClass, int attackPower, int skillsPower, int defense, Items userItems, String equippedWeapon, boolean isTutorialFinished) {
        super(x, y);
        this.name = name;
        this.experience = experience;
        this.level = level;
        this.HP = HP;
        this.heroClass = heroClass;
        this.attackPower = attackPower;
        this.skillsPower = skillsPower;
        this.defense = defense;
        this.userItems = userItems;
        this.equippedWeapon = equippedWeapon;
        this.isTutorialFinished = isTutorialFinished;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public String getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(String heroClass) {
        this.heroClass = heroClass;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }

    public int getSkillsPower() {
        return skillsPower;
    }

    public void setSkillsPower(int skillsPower) {
        this.skillsPower = skillsPower;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public Items getUserItems() {
        return userItems;
    }

    public void setUserItems(Items userItems) {
        this.userItems = userItems;
    }

    public String getEquippedWeapon() {
        return equippedWeapon;
    }

    public void setEquippedWeapon(String equippedWeapon) {
        this.equippedWeapon = equippedWeapon;
    }

    public boolean isTutorialFinished() {
        return isTutorialFinished;
    }

    public void setTutorialFinished(boolean tutorialFinished) {
        isTutorialFinished = tutorialFinished;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;
        Player player = (Player) o;
        return experience == player.experience &&
                level == player.level &&
                HP == player.HP &&
                attackPower == player.attackPower &&
                skillsPower == player.skillsPower &&
                defense == player.defense &&
                isTutorialFinished == player.isTutorialFinished &&
                Objects.equals(name, player.name) &&
                Objects.equals(heroClass, player.heroClass) &&
                Objects.equals(userItems, player.userItems) &&
                Objects.equals(equippedWeapon, player.equippedWeapon);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, experience, level, HP, heroClass, attackPower, skillsPower, defense, userItems, equippedWeapon, isTutorialFinished);
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", experience=" + experience +
                ", level=" + level +
                ", HP=" + HP +
                ", heroClass='" + heroClass + '\'' +
                ", attackPower=" + attackPower +
                ", skillsPower=" + skillsPower +
                ", defense=" + defense +
                ", userItems=" + userItems +
                ", equippedWeapon='" + equippedWeapon + '\'' +
                ", isTutorialFinished=" + isTutorialFinished +
                '}';
    }
}
