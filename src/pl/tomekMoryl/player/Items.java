package pl.tomekMoryl.player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Items implements Serializable {

    private static final long serialVersionUID = 5435498L;

    private List<String> weapons = new ArrayList<>();
    private List<String> heroUpgrades = new ArrayList<>();;
    private int potions = 0;
    private boolean pokeBall = false;
    private boolean holyHandGranade = false;
    private boolean moneyFromHolyCommunion = false;

    public List<String> getWeapons() {
        return weapons;
    }

    public void setWeapons(List<String> weapons) {
        this.weapons = weapons;
    }

    public List<String> getHeroUpgrades() {
        return heroUpgrades;
    }

    public void setHeroUpgrades(List<String> heroUpgrades) {
        this.heroUpgrades = heroUpgrades;
    }

    public int getPotions() {
        return potions;
    }

    public void setPotions(int potions) {
        this.potions = potions;
    }

    public boolean isPokeBall() {
        return pokeBall;
    }

    public void setPokeBall(boolean pokeBall) {
        this.pokeBall = pokeBall;
    }

    public boolean isHolyHandGranade() {
        return holyHandGranade;
    }

    public void setHolyHandGranade(boolean holyHandGranade) {
        this.holyHandGranade = holyHandGranade;
    }

    public boolean isMoneyFromHolyCommunion() {
        return moneyFromHolyCommunion;
    }

    public void setMoneyFromHolyCommunion(boolean moneyFromHolyCommunion) {
        this.moneyFromHolyCommunion = moneyFromHolyCommunion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Items)) return false;
        Items items = (Items) o;
        return potions == items.potions &&
                pokeBall == items.pokeBall &&
                holyHandGranade == items.holyHandGranade &&
                moneyFromHolyCommunion == items.moneyFromHolyCommunion &&
                Objects.equals(weapons, items.weapons) &&
                Objects.equals(heroUpgrades, items.heroUpgrades);
    }

    @Override
    public int hashCode() {

        return Objects.hash(weapons, heroUpgrades, potions, pokeBall, holyHandGranade, moneyFromHolyCommunion);
    }

    @Override
    public String toString() {
        return "Items{" +
                "weapons=" + weapons +
                ", heroUpgrades=" + heroUpgrades +
                ", potions=" + potions +
                ", pokeBall=" + pokeBall +
                ", holyHandGranade=" + holyHandGranade +
                ", moneyFromHolyCommunion=" + moneyFromHolyCommunion +
                '}';
    }
}
