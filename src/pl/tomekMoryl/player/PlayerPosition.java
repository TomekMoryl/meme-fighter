package pl.tomekMoryl.player;

import java.io.Serializable;

public class PlayerPosition implements Serializable {

    private static final long serialVersionUID = 44345498L;

    private int x;
    private int y;

    public PlayerPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "PlayerPosition{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
