package pl.tomekMoryl.player;

import pl.tomekMoryl.constants.DisplayConstants;
import pl.tomekMoryl.constants.HeroConstants;
import pl.tomekMoryl.constants.MessageColors;
import pl.tomekMoryl.gameMap.WorldMap;

import java.io.*;
import java.util.Scanner;

public class PlayerLogic {


    public static Player newGame() {
        DisplayConstants.clearScreen();
        Scanner scanner = new Scanner(System.in);
        Player player = new Player(1, 2);
        System.out.println("Pick you name");
        player.setName(scanner.nextLine());
        DisplayConstants.clearScreen();
        boolean isClassPicked = false;
        while (!isClassPicked) {
            System.out.println("    Chose your class \n");
            System.out.println(MessageColors.RED + "  (1) " + HeroConstants.KNIGHT + " attack power +2\n" +
                    MessageColors.CYAN + "  (2) " + HeroConstants.MAGE + " Skill power +3\n" +
                    MessageColors.MAGENTA + "  (3) " + HeroConstants.WARRIOR + " HP + 50 defense +2 \n"
            );
            String herroClass = scanner.nextLine();
            DisplayConstants.clearScreen();
            switch (herroClass) {
                case ("1"): {
                    player.setHeroClass(HeroConstants.KNIGHT);
                    System.out.println(HeroConstants.KNIGHT_IMAGE);
                    System.out.println("   You Picked " + HeroConstants.KNIGHT);
                    player.setAttackPower(player.getAttackPower() + 2);
                    DisplayConstants.waitForEnter();
                    isClassPicked = true;
                    break;
                }
                case ("2"): {
                    player.setHeroClass(HeroConstants.MAGE);
                    System.out.println(HeroConstants.MAGE_IMAGE);
                    System.out.println("   You Picked " + HeroConstants.MAGE);
                    player.setSkillsPower(player.getSkillsPower() + 3);
                    DisplayConstants.waitForEnter();
                    isClassPicked = true;
                    break;
                }
                case ("3"): {
                    player.setHeroClass(HeroConstants.WARRIOR);
                    System.out.println(HeroConstants.WARRIOR_IMAGE);
                    System.out.println("   You Picked " + HeroConstants.WARRIOR);
                    player.setDefense(player.getDefense() + 2);
                    player.setHP(player.getHP() + 50);
                    DisplayConstants.waitForEnter();
                    isClassPicked = true;
                    break;
                }
                default: {
                    System.out.println("\n     Wrong command, try again");
                    DisplayConstants.waitForEnter();
                }
            }
        }

        return player;
    }

    public static void levelUP(Player player, int experienceReward) {
        int playerExperience = player.getExperience() + experienceReward;
            player.setExperience(playerExperience);
        if (Math.pow(player.getLevel(), 2) * 40 < playerExperience) {
            DisplayConstants.clearScreen();
            player.setHP(player.getHP() + 50);
            player.setLevel(player.getLevel() + 1);
            Scanner scanner = new Scanner(System.in);
            System.out.println("Level Up now your level is: " + player.getLevel());
            System.out.println("Now your have: " + player.getHP() + " HP");
            boolean isPointSpend = false;
            while (!isPointSpend) {
                System.out.println("Type attribute you want to upgrade");
                System.out.println(MessageColors.RED + "attack (+2)" + MessageColors.YELLOW + "skill (+2)" + MessageColors.MAGENTA +
                        "defense (+3)" + MessageColors.RESET);
                String skillChoice = scanner.nextLine();
                DisplayConstants.clearScreen();
                switch (skillChoice) {
                    case ("attack"): {
                        player.setAttackPower(player.getAttackPower() + 2);
                        System.out.println("You upgraded your attack power now you have " + player.getAttackPower() + " points");
                        isPointSpend = true;
                        DisplayConstants.waitForEnter();
                        break;
                    }
                    case ("skill"): {
                        player.setSkillsPower(player.getSkillsPower() + 2);
                        System.out.println("You upgraded your attack power now you have " + player.getSkillsPower() + " points");
                        isPointSpend = true;
                        DisplayConstants.waitForEnter();
                        break;
                    }
                    case ("defense"): {
                        player.setDefense(player.getDefense() + 3);
                        System.out.println("You upgraded your defense now you have " + player.getDefense() + " points");
                        isPointSpend = true;
                        DisplayConstants.waitForEnter();
                        break;
                    }
                    default: {
                        System.out.println("Wrong Command try again");
                        DisplayConstants.waitForEnter();
                    }
                }

            }
        }
    }

    public static Player loadGame(WorldMap worldMap, PlayerPosition playerPosition) {
        try {
            FileInputStream playerFile = new FileInputStream(new File("Player.txt"));
            FileInputStream mapFile = new FileInputStream(new File("Map.txt"));
            FileInputStream positionFile = new FileInputStream(new File("Position.txt"));
            ObjectInputStream playerInput = new ObjectInputStream(playerFile);
            ObjectInputStream mapInput = new ObjectInputStream(mapFile);
            ObjectInputStream positionInput = new ObjectInputStream(positionFile);

            // Read objects
            Player player = (Player) playerInput.readObject();
            PlayerPosition playerPositionFromFile = (PlayerPosition) positionInput.readObject();

            playerPosition.setX(playerPositionFromFile.getX());
            playerPosition.setY(playerPositionFromFile.getY());

            WorldMap worldMapFromFile = (WorldMap) mapInput.readObject();
            worldMap.setWorldMap(worldMapFromFile.getWorldMap());

            playerFile.close();
            mapFile.close();
            positionFile.close();
            playerInput.close();
            mapInput.close();
            positionInput.close();
            return player;
        } catch (FileNotFoundException e) {
            System.out.println("No file " + e);
        } catch (IOException e) {
            System.out.println("Input Output Expectation " + e);
        } catch (ClassNotFoundException e) {
            System.out.println("No Class Found " + e);
        }
        return new Player(1, 2);
    }
}
