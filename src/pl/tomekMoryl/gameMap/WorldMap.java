package pl.tomekMoryl.gameMap;

import pl.tomekMoryl.constants.DisplayConstants;
import pl.tomekMoryl.constants.HeroConstants;
import pl.tomekMoryl.gameMap.logic.FieldColors;
import pl.tomekMoryl.gameMap.logic.ReadMap;
import pl.tomekMoryl.player.PlayerPosition;

import java.io.Serializable;

public class WorldMap implements Serializable {

    private static final long serialVersionUID = 5321522398L;

    private char[][] worldMap = new ReadMap().populateMapWithFields();

    public void printMap(PlayerPosition playerPosition) {
        for (int x = 0; x < 32; x++) {
            for (int y = 0; y < 64; y++) {
                System.out.print(FieldColors.paintMapField(worldMap[x][y]) + worldMap[x][y]);
            }
            worldMap[playerPosition.getX()][playerPosition.getY()] = HeroConstants.HERO_ICON;
            if (x < DisplayConstants.mapLegends.size()) {
                System.out.print("     " + DisplayConstants.mapLegends.get(x));
            }
            System.out.println();
        }
    }

    public void setWorldMap(char[][] worldMap) {
        this.worldMap = worldMap;
    }

    public char[][] getWorldMap() {
        return worldMap;
    }

    public void setNewField(int xAxis, int yAxis, char newCharacter) {
        worldMap[xAxis][yAxis] = newCharacter;
    }

}
