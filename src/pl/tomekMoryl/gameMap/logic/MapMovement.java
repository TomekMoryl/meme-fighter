package pl.tomekMoryl.gameMap.logic;

import pl.tomekMoryl.constants.DisplayConstants;
import pl.tomekMoryl.constants.MapMovementCommands;
import pl.tomekMoryl.gameMap.WorldMap;
import pl.tomekMoryl.inputHandling.ReadInput;
import pl.tomekMoryl.player.Player;
import pl.tomekMoryl.player.PlayerPosition;

import java.io.*;

public class MapMovement {
    public static char previousField = '.';
    private MapEvents mapEvents = new MapEvents();

    public String makeMove(WorldMap worldMap, PlayerPosition playerPosition, Player player) {
        int xAxis = playerPosition.getX();
        int yAxis = playerPosition.getY();
        char nextIcon;

        switch (ReadInput.readKeyboardButton()) {
            case (MapMovementCommands.MOVE_DOWN):
                nextIcon = worldMap.getWorldMap()[xAxis + 1][yAxis];
                if (mapEvents.isPossibleToMove(nextIcon, player, xAxis + 1, yAxis)) {
                    worldMap.setNewField(xAxis, yAxis, previousField);
                    previousField = worldMap.getWorldMap()[xAxis + 1][yAxis];
                    worldMap.setNewField(xAxis + 1, yAxis, 'H');
                    playerPosition.setX(xAxis + 1);
                    mapEvents.isEventOnTheField(nextIcon, player, playerPosition);
                    return ":), nice";
                } else {
                    return ":/, whatever";
                }
            case (MapMovementCommands.MOVE_UP):
                nextIcon = worldMap.getWorldMap()[xAxis - 1][yAxis];
                if (mapEvents.isPossibleToMove(nextIcon, player, xAxis - 1, yAxis)) {
                    worldMap.setNewField(xAxis, yAxis, previousField);
                    previousField = worldMap.getWorldMap()[xAxis - 1][yAxis];
                    worldMap.setNewField(xAxis - 1, yAxis, 'H');
                    playerPosition.setX(xAxis - 1);
                    mapEvents.isEventOnTheField(nextIcon, player, playerPosition);
                    return ":), Good Job ";
                } else {
                    return ":|, Nooope";
                }
            case (MapMovementCommands.MOVE_LEFT):
                nextIcon = worldMap.getWorldMap()[xAxis][yAxis - 2];
                if (mapEvents.isPossibleToMove(nextIcon, player, xAxis, yAxis - 2)) {
                    worldMap.setNewField(xAxis, yAxis, previousField);
                    previousField = worldMap.getWorldMap()[xAxis][yAxis - 2];
                    worldMap.setNewField(xAxis, yAxis - 2, 'H');
                    playerPosition.setY(yAxis - 2);
                    mapEvents.isEventOnTheField(nextIcon, player, playerPosition);
                    return "^^, you're the best ";
                } else {
                    return ":<, what are you doing";
                }
            case (MapMovementCommands.MOVE_RIGHT):
                nextIcon = worldMap.getWorldMap()[xAxis][yAxis + 2];
                if (mapEvents.isPossibleToMove(nextIcon, player, xAxis, yAxis + 2)) {
                    worldMap.setNewField(xAxis, yAxis, previousField);
                    previousField = worldMap.getWorldMap()[xAxis][yAxis + 2];
                    worldMap.setNewField(xAxis, yAxis + 2, 'H');
                    playerPosition.setY(yAxis + 2);
                    mapEvents.isEventOnTheField(nextIcon, player, playerPosition);
                    return ";), no pain no gain";
                } else {
                    return ":(, no";
                }
            case (MapMovementCommands.INTERACTION):
                Interaction.randomChuckNorrisJoke();
                return "Just Fun time";
            case (MapMovementCommands.ITEM):
                Interaction.showItems(player);
                return "Just looking into my inventory";
            case (MapMovementCommands.SAVE):
                try {
                    FileOutputStream playerSave = new FileOutputStream(new File("Player.txt"));
                    FileOutputStream mapSave = new FileOutputStream(new File("Map.txt"));
                    FileOutputStream positionSave = new FileOutputStream(new File("Position.txt"));
                    ObjectOutputStream playerOutput = new ObjectOutputStream(playerSave);
                    ObjectOutputStream mapOutput = new ObjectOutputStream(mapSave);
                    ObjectOutputStream positionOutput = new ObjectOutputStream(positionSave);
                    playerOutput.writeObject(player);
                    mapOutput.writeObject(worldMap);
                    positionOutput.writeObject(playerPosition);
                    playerOutput.close();
                    mapOutput.close();
                    positionOutput.close();
                    playerSave.close();
                    mapSave.close();
                    positionSave.close();
                    DisplayConstants.clearScreen();
                    System.out.println("     Game Saved    ");
                    DisplayConstants.waitForEnter();
                    return " No Move, only save :) ";
                } catch (FileNotFoundException e) {
                    System.out.println("No File Found " + e);
                } catch (IOException e) {
                    System.out.println("Input Output error " + e);
                }

            default: {
                return "Don't know this word";
            }
        }
    }
}
