package pl.tomekMoryl.gameMap.logic;

import pl.tomekMoryl.constants.MessageColors;

public class FieldColors {

    public static String paintMapField(char field) {
        switch (field) {
            case ('M'):{
                return MessageColors.RED;
            }
            case ('W'): {
                return MessageColors.BLUE;
            }
            case ('C'): {
                return MessageColors.MAGENTA;
            }
            case ('U'): {
                return MessageColors.BRIGHT_MAGENTA;
            }
            case ('E'): {
                return MessageColors.BRIGHT_RED;
            }
            case ('#'): {
                return MessageColors.GREEN;
            }
            case ('B'): {
                return MessageColors.YELLOW;
            }
            case ('S'): {
                return MessageColors.WHITE;
            }
            case ('@'): {
                return MessageColors.CYAN;
            }
            case ('H'): {
                return MessageColors.BRIGHT_CYAN;
            }
            case ('.'): {
                return MessageColors.BRIGHT_GREEN;
            }
            case ('N'): {
                return MessageColors.YELLOW;
            }
            case ('*'): {
                return MessageColors.BRIGHT_YELLOW;
            }
            default: {
                return MessageColors.RESET;
            }
        }
    }
}
