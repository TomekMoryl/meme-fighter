package pl.tomekMoryl.gameMap.logic;

import pl.tomekMoryl.constants.DisplayConstants;
import pl.tomekMoryl.events.BlockPathEvent;
import pl.tomekMoryl.events.Events;
import pl.tomekMoryl.fightSystem.Fight;
import pl.tomekMoryl.player.Player;
import pl.tomekMoryl.player.PlayerPosition;

public class MapEvents {
    private BlockPathEvent blockPathEvent = new BlockPathEvent();
    private Events events = new Events();
    private Fight fight = new Fight();

    public boolean isPossibleToMove(char field, Player player, int newXAxis, int newYAxis) {
        DisplayConstants.clearScreen();

        switch (field) {
            case ('M'):
            case ('W'): {
                return false;
            }
            case ('N'):
            case ('S'): {
                return blockPathEvent.isMoveBlocked(field, player, newXAxis, newYAxis);
            }

            default:
                return true;
        }
    }

    public void isEventOnTheField(char field, Player player, PlayerPosition newPlayerPosition) {
        DisplayConstants.clearScreen();
        switch (field) {
            case ('N'):
            case ('U'):
            case ('C'):
            case ('E'):
            case ('S'):
            case ('B'):
            case ('@'): {
                events.eventLoader(field, player, newPlayerPosition);
                break;
            }
            case ('.'): {
                if (player.isTutorialFinished() && Math.random() > 0.97) {
                    fight.randomFight(player);
                    break;
                }
            }
            case ('#'): {
                if (player.isTutorialFinished() && Math.random() > 0.96) {
                    fight.randomFight(player);
                    break;
                }
            }
            case ('*'): {
                if (player.isTutorialFinished() && Math.random() > 0.94) {
                    fight.randomFight(player);
                    break;
                }
            }
            default:
        }
    }
}
