package pl.tomekMoryl.gameMap.logic;

import pl.tomekMoryl.constants.DisplayConstants;
import pl.tomekMoryl.constants.FileConstants;
import pl.tomekMoryl.constants.MessageColors;
import pl.tomekMoryl.fileHandling.ReadFile;
import pl.tomekMoryl.player.Player;


public class Interaction {
    private static ReadFile readFile = new ReadFile();

    public static void randomChuckNorrisJoke() {
        String jokes = readFile.fileReader(FileConstants.INTERACTION);
        String[] jokeList = jokes.split("\n");
        DisplayConstants.clearScreen();
        System.out.println(DisplayConstants.CHUCK_NORRIS);
        System.out.println(MessageColors.RED + "Random Chuck Norris Joke:\n" + MessageColors.RESET);
        System.out.println(jokeList[(int) Math.floor(Math.random() * jokeList.length)]);
        DisplayConstants.waitForEnter();
    }

    public static void showItems(Player player) {
        System.out.println(MessageColors.RED + "Your items and stats:\n" + MessageColors.RESET);
        System.out.println("Payer Name: " + player.getName() + "\nLevel: " + player.getLevel() + "\nWeapon :"
                + player.getEquippedWeapon() + "\nHealth Point: " + player.getHP() + "\nAttack Power: "
                + player.getAttackPower() + "\nSkill Power: " + player.getSkillsPower() + "\nDefense: "
                + player.getDefense());
        if (player.getUserItems().getPotions() > 0)
            System.out.println("Potions: " + player.getUserItems().getPotions());
        if (player.getUserItems().isPokeBall()) System.out.println("Pokeball available");
        if (player.getUserItems().isHolyHandGranade()) System.out.println("Holy Hand Grenade available");
        if (player.getUserItems().isMoneyFromHolyCommunion()) System.out.println("Money from holy communion available");
        DisplayConstants.waitForEnter();
    }
}
