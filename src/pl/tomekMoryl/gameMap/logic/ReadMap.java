package pl.tomekMoryl.gameMap.logic;

import pl.tomekMoryl.constants.FileConstants;
import pl.tomekMoryl.fileHandling.ReadFile;

public class ReadMap {

    private char[][] worldMap = new char[32][64];
    private ReadFile readFile = new ReadFile();

    public char[][] populateMapWithFields() {
        int i = 0;
        String mapString = readFile.fileReader(FileConstants.MAP_FILE_NAME);
        for (int x = 0; x < 32; x++) {
            for (int y = 0; y < 64; y++) {
                worldMap[x][y] = mapString.charAt(i);
                i++;
            }
        }
        return worldMap;
    }
}
