package pl.tomekMoryl.constants;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class DisplayConstants {
    private final static String HERO = MessageColors.BRIGHT_CYAN + "H" + MessageColors.RESET + " - Hero ";
    private final static String MOUNTAINS = MessageColors.RED + "M" + MessageColors.RESET + " - Mountain ";
    private final static String WATER = MessageColors.BLUE + "W" + MessageColors.RESET + " - Water ";
    private final static String ENEMY = MessageColors.BRIGHT_RED + "E" + MessageColors.RESET + " - Enemy ";
    private final static String UPGRADE = MessageColors.BRIGHT_MAGENTA + "U" + MessageColors.RESET + " - Upgrade ";
    private final static String CHEST = MessageColors.MAGENTA + "C" + MessageColors.RESET + " - Chest ";
    private final static String FORREST = MessageColors.GREEN + "#" + MessageColors.RESET + " - Forrest ";
    private final static String BRIDGE = MessageColors.YELLOW + "B" + MessageColors.RESET + " - Bridge ";
    private final static String STONE = MessageColors.WHITE + "S" + MessageColors.RESET + " - Stone ";
    private final static String WARLORD = MessageColors.CYAN + "@" + MessageColors.RESET + " - Warlord ";
    private final static String GRASS = MessageColors.BRIGHT_GREEN + "." + MessageColors.RESET + " - Grass ";
    private final static String NPC = MessageColors.YELLOW + "N" + MessageColors.RESET + " - NPC ";
    private final static String DESSERT = MessageColors.BRIGHT_YELLOW + "*" + MessageColors.RESET + " - Dessert ";
    private final static String EMPTY = "";
    private final static String CONTROL = "Commands:";
    private final static String UP = "up";
    private final static String DOWN = "down";
    private final static String RIGHT = "right";
    private final static String LEFT = "left";
    private final static String INTERACTION = "interaction";
    private final static String SAVE = "save";
    private final static String ITEMS = "item";

    public final static List<String> mapLegends = Arrays.asList(HERO, MOUNTAINS, WATER, ENEMY, UPGRADE, CHEST, NPC, BRIDGE,
            STONE, WARLORD, GRASS, FORREST, DESSERT, EMPTY, CONTROL, UP, DOWN, LEFT, RIGHT, INTERACTION, ITEMS, SAVE);

    public final static void waitForEnter() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(MessageColors.GREEN + "                 <<Press Enter>>" + MessageColors.RESET);
        scanner.nextLine();
        clearScreen();
    }

    public final static String menuLogo =
            "  __  __ ______ __  __ ______   ______ _____ _____ _    _ _______ ______ _____  \n" +
                    " |  \\/  |  ____|  \\/  |  ____| |  ____|_   _/ ____| |  | |__   __|  ____|  __ \\ \n" +
                    " | \\  / | |__  | \\  / | |__    | |__    | || |  __| |__| |  | |  | |__  | |__) |\n" +
                    " | |\\/| |  __| | |\\/| |  __|   |  __|   | || | |_ |  __  |  | |  |  __| |  _  / \n" +
                    " | |  | | |____| |  | | |____  | |     _| || |__| | |  | |  | |  | |____| | \\ \\ \n" +
                    " |_|  |_|______|_|  |_|______| |_|    |_____\\_____|_|  |_|  |_|  |______|_|  \\_\\\n" +
                    "                                                                                \n" +
                    "                                                                                ";

    public final static void setTimeout(int timeout) {
        try {
            Thread.sleep(timeout);
            clearScreen();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void fightIntro() {
        DisplayConstants.setTimeout(100);
        System.out.println("\n" +
                ".______   .______       _______ .______      ___      .______       _______ \n" +
                "|   _  \\  |   _  \\     |   ____||   _  \\    /   \\     |   _  \\     |   ____|\n" +
                "|  |_)  | |  |_)  |    |  |__   |  |_)  |  /  ^  \\    |  |_)  |    |  |__   \n" +
                "|   ___/  |      /     |   __|  |   ___/  /  /_\\  \\   |      /     |   __|  \n" +
                "|  |      |  |\\  \\----.|  |____ |  |     /  _____  \\  |  |\\  \\----.|  |____ \n" +
                "| _|      | _| `._____||_______|| _|    /__/     \\__\\ | _| `._____||_______|\n" +
                "                                                                            ");
        DisplayConstants.setTimeout(700);
        System.out.println("" +
                "\n" +
                " _______   ______   .______      \n" +
                "|   ____| /  __  \\  |   _  \\     \n" +
                "|  |__   |  |  |  | |  |_)  |    \n" +
                "|   __|  |  |  |  | |      /     \n" +
                "|  |     |  `--'  | |  |\\  \\----.\n" +
                "|__|      \\______/  | _| `._____|");
        DisplayConstants.setTimeout(700);
        System.out.println("" + MessageColors.RED +
                "\n" +
                "                                                        \n" +
                "88888888888 88   ,ad8888ba,  88        88 888888888888  \n" +
                "88          88  d8\"'    `\"8b 88        88      88       \n" +
                "88          88 d8'           88        88      88       \n" +
                "88aaaaa     88 88            88aaaaaaaa88      88       \n" +
                "88\"\"\"\"\"     88 88      88888 88\"\"\"\"\"\"\"\"88      88       \n" +
                "88          88 Y8,        88 88        88      88       \n" +
                "88          88  Y8a.    .a88 88        88      88       \n" +
                "88          88   `\"Y88888P\"  88        88      88       " + MessageColors.RESET);
        DisplayConstants.setTimeout(1000);
    }

    public static void gameIntro() {
        String gameLogo =
                "ooo        ooooo oooooooooooo ooo        ooooo oooooooooooo                            \n" +
                        "`88.       .888' `888'     `8 `88.       .888' `888'     `8                            \n" +
                        " 888b     d'888   888          888b     d'888   888                                    \n" +
                        " 8 Y88. .P  888   888oooo8     8 Y88. .P  888   888oooo8                               \n" +
                        " 8  `888'   888   888    \"     8  `888'   888   888    \"                               \n" +
                        " 8    Y     888   888       o  8    Y     888   888       o                            \n" +
                        "o8o        o888o o888ooooood8 o8o        o888o o888ooooood8                            \n" +
                        "                                                                                       \n" +
                        "                                                                                       \n" +
                        "                                                                                       \n" +
                        "oooooooooooo ooooo   .oooooo.    ooooo   ooooo ooooooooooooo oooooooooooo ooooooooo.   \n" +
                        "`888'     `8 `888'  d8P'  `Y8b   `888'   `888' 8'   888   `8 `888'     `8 `888   `Y88. \n" +
                        " 888          888  888            888     888       888       888          888   .d88' \n" +
                        " 888oooo8     888  888            888ooooo888       888       888oooo8     888ooo88P'  \n" +
                        " 888    \"     888  888     ooooo  888     888       888       888    \"     888`88b.    \n" +
                        " 888          888  `88.    .88'   888     888       888       888       o  888  `88b.  \n" +
                        "o888o        o888o  `Y8bood8P'   o888o   o888o     o888o     o888ooooood8 o888o  o888o \n" +
                        "                                                                                       ";

        String[] logoLines = gameLogo.split("\n");
        clearScreen();

        for (int i = 0; i < logoLines.length; i++) {
            System.out.println(logoLines[i]);
            DisplayConstants.setTimeout(200 - ((i + 1) * 10));
        }

        DisplayConstants.waitForEnter();

    }

    public static final String CHUCK_NORRIS =
            "                            MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN\n" +
                    "                           MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN\n" +
                    "                           MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN\n" +
                    "      NM                  MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\n" +
                    "      MMMMM              MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\n" +
                    "       MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM\n" +
                    "        MMMMMMMMMMMMMMMMMMMMMMMMMM8MMMMMMMMMIMMMMM8,. ...........OMMMMMMMMMMMMMMMMMMMMMMMMMMMM\n" +
                    "           MMMMMMMMMMMMMMMMMMMMMMM ..N. .....~MMMM...............:MMMMNMMMMMMMMMMMMMMMMMMMMMMM\n" +
                    "             NMMMMMMMMMMMMMMMMMMMMM.....:..DMMMMMNZ Z.... .......M$MMMMMMMMMMMMMMMMMMMMMMMMMMM\n" +
                    "                 MMMMMMMMMMNMMMMMMM....... 7=MMMMMMO....Z .......MM7MMMMMMMMMMMMMMMMMMMMMMMMM\n" +
                    "                    MMMMMMMMMMMMMMMMM  Z...MMMZ .. .,M..,........MMMMMMMMMMMMMMMMMMMMMMMMMMMM\n" +
                    "                        MMMMMM.......DOM ....N7..................MMMMMMMMMMMMMMMMMMMMMMMMMMM\n" +
                    "                           MMM....... M. ... .  ... ..............M...$MMMMMMMMMMMMMMMMMMMM\n" +
                    "                             ........... ........~. ..............M..=....+MMMMMMMMMMMMMM\n" +
                    "                             ......+.NMI~........ . ..............M.,.I...MMMMMMMMMMMMMMN\n" +
                    "                             ......$... ...... O..................,.....$MMMMMMMMMMMMN\n" +
                    "                             .....M.......... M M.. .............. 8  .OMMMMMMMMMMMN\n" +
                    "                              ..=7I,,.,,IMI...M.................. ..MMMMMMMMMMM\n" +
                    "                              ....DMMMMMMMMMMMMMMMO..............D...MMMMMMMMM\n" +
                    "                               .MMMMMMMMMMMMMMDDMM:,N..............DMMMMMMMMMMM\n" +
                    "                               NMMMMMNMM8 . .... ...,~............  MMMMMMMMM\n" +
                    "                               MMMMM,:......::~..M8M8MM...............MMMMMM\n" +
                    "                               MMMM ... . .........,MM..............MMMMMO$\n" +
                    "                               MMMMM... =.=. .. . . MM ....... . ...MMMMMMM\n" +
                    "                                NMMMMMMMMMM?  ..O.?NM7 ....... ......MMMMMM\n" +
                    "                                 NMMMMMMMMMMMMMMMMM........  $ . ...MMMNMMM\n" +
                    "                                  MMMMMMMMMMMMMMM.........,, ......MMMMMMMM\n" +
                    "                                   OMMMMMMMM8 , .. .. .,N.... ...:MMMMMMMMMMN\n" +
                    "                                    MMMMMMMM?N. ...~MD.:MNI8MMMMMMMMMMMMMMMMMN\n" +
                    "                               MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN\n" +
                    "                            NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN\n";


    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void gameStart() {
        clearScreen();
        System.out.println("\n\n  Hello, you need to kill enemy that is marked with '@' simple as that. \n" +
                "  - Use commands on the right side of the map.\n" +
                "   - There will be random encounters depending on what type of terrain you are moving.\n" +
                "   - Now go and get me that job");
        waitForEnter();
        System.out.println("............................................________ \n" +
                "....................................,.-'\"...................``~., \n" +
                ".............................,.-\"...................................\"-., \n" +
                ".........................,/...............................................\":, \n" +
                ".....................,?......................................................, \n" +
                ".................../...........................................................,} \n" +
                "................./......................................................,:`^`..} \n" +
                ".............../...................................................,:\"........./ \n" +
                "..............?.....__.........................................:`.........../ \n" +
                "............./__.(.....\"~-,_..............................,:`........../ \n" +
                ".........../(_....\"~,_........\"~,_....................,:`........_/ \n" +
                "..........{.._$;_......\"=,_.......\"-,_.......,.-~-,},.~\";/....} \n" +
                "...........((.....*~_.......\"=-._......\";,,./`..../\"............../ \n" +
                "...,,,___.`~,......\"~.,....................`.....}............../ \n" +
                "............(....`=-,,.......`........................(......;_,,-\" \n" +
                "............/.`~,......`-...................................../ \n" +
                ".............`~.*-,.....................................|,./.....,__ \n" +
                ",,_..........}.>-._...................................|..............`=~-, \n" +
                ".....`=~-,__......`,................................. \n" +
                "...................`=~-,,.,............................... \n" +
                "................................`:,,...........................`..............__ \n" +
                ".....................................`=-,...................,%`>--==`` \n" +
                "........................................_..........._,-%.......` \n" +
                "..................................., ");
        setTimeout(2000);
        System.out.println("I meant go and save the world");
        waitForEnter();
    }

    public static void gameFinal() {
        clearScreen();
        System.out.println("\n    Thank you for completing this game, I hope you enjoyed playing it ");
        System.out.println("       /~~\\________________________/~~\\\n" +
                "      ! oo !          42           !   !\n" +
                "       \\__/~~~~~~~~~~~~~~~~~~~~~~~~~\\_/\n" +
                "          \\  Dear Hero               \\\n" +
                "            \\ You Beat that game       \\\n" +
                "              \\                          \\\n" +
                "                \\  \"Thank you for your     \\\n" +
                "                  \\  patience, and time      \\\n" +
                "                    \\  for playing to the      \\\n" +
                "                      \\  end, I hope you         \\\n" +
                "                        \\  enjoyed it ( ͡° ͜ʖ ͡°)   \\\n" +
                "                      /~~\\___________________________~~\\\n" +
                "                     ! oo !          42              !  !\n" +
                "                      \\__/~~~~~~~~~~~~~~~~~~~~~~~~~~~\\_/");
        waitForEnter();
    }
}
