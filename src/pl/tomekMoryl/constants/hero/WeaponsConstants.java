package pl.tomekMoryl.constants.hero;

public class WeaponsConstants {
    public static final String SMALL_STAFF = "small staff";
    public static final String MEDIUM_STAFF = "medium staff";
    public static final String BIG_STAFF = "big staff";
    public static final String PEW_PEW_STAFF = "pew pew staff";
    public static final String SMALL_SWORD = "small sword";
    public static final String MEDIUM_SWORD = "medium sword";
    public static final String BIG_SWORD = "big sword";
    public static final String CHA_CHING_SWORD = "cha ching sword";
    public static final String SMALL_AXE = "small axe";
    public static final String MEDIUM_AXE = "medium axe";
    public static final String BIG_AXE = "big axe";
    public static final String SHUSH_AXE = "shush axe";

    public static final String SMALL_SWORD_IMAGE =
            "          -\n" +
                    "          |\n" +
                    "O=========|>>>>>>>>>>>>>>>>>>>>>>>>>>\n" +
                    "          |\n" +
                    "          -";
    public static final String SMALL_SWORD_DIALOG = "Sword like sword what can I say ° ͜ʖ ͡ ";

    public static final String SMALL_STAFF_IMAGE =
            "   + ,*\n" +
                    "  _  /   <-Small magic wand\n" +
                    " ' `-' +\n" +
                    "   /  +";
    public static final String SMALL_STAFF_DIALOG = "How do you feel, Mr Potter ?";

    public static final String SMALL_AXE_IMAGE =
            " _,-,\n" +
                    "T_  |\n" +
                    "||`-'\n" +
                    "||\n" +
                    "||\n" +
                    "~~";

    public static final String SMALL_AXE_DIALOG = "Wow, are you a dwarf ?";

    public static final String MEDIUM_SWORD_IMAGE =
            "    /^\\\n" +
                    "   |\\ /|\n" +
                    "   | | |\n" +
                    "   | | |\n" +
                    "   | | |\n" +
                    "   | | |\n" +
                    "   | | |\n" +
                    "    |||\n" +
                    "    |||\n" +
                    "    |||\n" +
                    "..  |||  ..\n" +
                    "`\\\\=====//'\n" +
                    "    (=)\n" +
                    "    (=)\n" +
                    "    (=)\n" +
                    "    \\U/";
    public static final String MEDIUM_SWORD_DIALOG = "Sword like sword what can I say ° ͜ʖ ͡ ";

    public static final String MEDIUM_STAFF_IMAGE =
            "   + ,*\n" +
                    "  _  /   <-Medium magic wand\n" +
                    " ' `-' +\n" +
                    "   /  +";
    public static final String MEDIUM_STAFF_DIALOG = "Amazing weapon, sir";

    public static final String MEDIUM_AXE_IMAGE =
            "          A\n" +
                    "         /!\\\n" +
                    "        / ! \\\n" +
                    " /\\     )___(\n" +
                    "(  `.____(_)_________\n" +
                    "|           __..--\"\"\n" +
                    "(       _.-|\n" +
                    " \\    ,' | |\n" +
                    "  \\  /   | |\n" +
                    "   \\(    | |\n" +
                    "    `    | |\n" +
                    "         | |";

    public static final String MEDIUM_AXE_DIALOG = "This is for you, good luck sir";

    public static final String BIG_SWORD_IMAGE =
            "       .\n" +
                    "        / \\\n" +
                    "       /   \\\n" +
                    "      /  |  \\\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    "      |  |  |\n" +
                    " ____/|  |  |\\____\n" +
                    "/____/\\___/\\____/ )\n" +
                    "(_______________)/\n" +
                    "      |/////|\n" +
                    "      |/////|\n" +
                    "      |/////|\n" +
                    "      |/////|\n" +
                    "      |/////|\n" +
                    "      |/////|\n" +
                    "      |/////|\n" +
                    "     (_______)";

    public static final String BIG_SWORD_DIALOG = "It's a big sword";

    public static final String BIG_STAFF_IMAGE =
            "   + ,*\n" +
                    "  _  /   <-Big magic wand\n" +
                    " ' `-' +\n" +
                    "   /  +";
    public static final String BIG_STAFF_DIALOG = "It's the same like previous one only bigger";

    public static final String BIG_AXE_IMAGE =
            "                   /^\\\n" +
                    "          _.-`:   /   \\   :'-._\n" +
                    "        ,`    :  |     |  :    '.\n" +
                    "      ,`       \\,|     |,/       '.\n" +
                    "     /           `-...-`           \\\n" +
                    "    :              .'.              :\n" +
                    "    |             . ' .             |\n" +
                    "    |             ' . '             |\n" +
                    "    :              '.'              :\n" +
                    "     \\           ,-\"\"\"-,           /\n" +
                    "      `.       /'|     |'\\       ,'\n" +
                    "        `._   ;  |     |  ;   _,'\n" +
                    "           `-.:  |     |  :,-'\n" +
                    "                 |     |\n" +
                    "                 |     |\n" +
                    "barbarian        |     |\n" +
                    "battle           |     |\n" +
                    "axe-head         |     |";

    public static final String BIG_AXE_DIALOG = "Big Axe, good for cutting";


    public static final String CHA_CHING_SWORD_IMAGE =
            "                 /\\\n" +
                    "                /  \\\n" +
                    "               / /\\ \\\n" +
                    "              / /  \\ \\\n" +
                    "             / /    \\ \\\n" +
                    "            /_/      \\_\\\n" +
                    "            \\    '`    /\n" +
                    "             )   ||   (\n" +
                    "             |   ||   |\n" +
                    "             |   ||   |\n" +
                    "             |   ||   |\n" +
                    "             |   ||   |\n" +
                    "             |   ||   |\n" +
                    "             |   ||   |\n" +
                    "             |   ||   |\n" +
                    "             |   ||   |\n" +
                    "             |   ||   |\n" +
                    " /           |   ||   |           \\\n" +
                    "/(           |   ||   |           )\\\n" +
                    "|`\\_         |   ||   |         _/'|\n" +
                    "|`. `-._     |   ||   |     _,-' ,'|\n" +
                    "(   ` . `-._ |  _--_  | _,-' , '   )\n" +
                    " `.._   ` . `-./.__.\\.-' , '   _,-'\n" +
                    "     `-._   ` | /  \\ | '   _,-'\n" +
                    "         `-._/ |_()_| \\_,-'\n" +
                    "      ___.-'   ______   `-,\n" +
                    "     '-----.  |______|   /\n" +
                    "            \\  ______   /\n" +
                    "            |  \\>    |>\n" +
                    "            <|   <   >|\n" +
                    "              `.____.'\n" +
                    "                V   V";
    public static final String SHUSH_AXE_IMAGE =
            "                      .~      ,   . ~.\n" +
                    "                     /                \\\n" +
                    "                    /      /~\\/~\\   ,  \\\n" +
                    "                   |   .   \\    /   '   |\n" +
                    "                   |         \\/         |\n" +
                    "          XX       |  /~~\\        /~~\\  |       XX\n" +
                    "        XX  X      | |  o  \\    /  o  | |      X  XX\n" +
                    "      XX     X     |  \\____/    \\____/  |     X     XX\n" +
                    " XXXXX     XX      \\         /\\        ,/      XX     XXXXX\n" +
                    "X        XX%;;@      \\      /  \\     ,/      @%%;XX        X\n" +
                    "X       X  @%%;;@     |           '  |     @%%;;@  X       X\n" +
                    "X      X     @%%;;@   |. ` ; ; ; ;  ,|   @%%;;@     X      X\n" +
                    " X    X        @%%;;@                  @%%;;@        X    X\n" +
                    "  X   X          @%%;;@              @%%;;@          X   X\n" +
                    "   X  X            @%%;;@          @%%;;@            X  X\n" +
                    "    XX X             @%%;;@      @%%;;@             X XX\n" +
                    "      XXX              @%%;;@  @%%;;@              XXX\n" +
                    "                         @%%;;%%;;@\n" +
                    "                           @%%;;@\n" +
                    "                         @%%;;@..@@\n" +
                    "                          @@@  @@@";
    public static final String PEW_PEW_STAFF_IMAGE =
            "                     \"   + ,*\\n\" +\n" +
                    "                    \"  _  /   <-pew pew magic wand\\n\" +\n" +
                    "                    \" ' `-' +\\n\" +\n" +
                    "                    \"   /  +\";";

    public static final String POKEBALL =
            "────────▄███████████▄────────\n" +
                    "─────▄███▓▓▓▓▓▓▓▓▓▓▓███▄─────\n" +
                    "────███▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓███────\n" +
                    "───██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██───\n" +
                    "──██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██──\n" +
                    "─██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██─\n" +
                    "██▓▓▓▓▓▓▓▓▓███████▓▓▓▓▓▓▓▓▓██\n" +
                    "██▓▓▓▓▓▓▓▓██░░░░░██▓▓▓▓▓▓▓▓██\n" +
                    "██▓▓▓▓▓▓▓██░░███░░██▓▓▓▓▓▓▓██\n" +
                    "███████████░░███░░███████████\n" +
                    "██░░░░░░░██░░███░░██░░░░░░░██\n" +
                    "██░░░░░░░░██░░░░░██░░░░░░░░██\n" +
                    "██░░░░░░░░░███████░░░░░░░░░██\n" +
                    "─██░░░░░░░░░░░░░░░░░░░░░░░██─\n" +
                    "──██░░░░░░░░░░░░░░░░░░░░░██──\n" +
                    "───██░░░░░░░░░░░░░░░░░░░██───\n" +
                    "────███░░░░░░░░░░░░░░░███────\n" +
                    "─────▀███░░░░░░░░░░░███▀─────\n" +
                    "────────▀███████████▀────────";

    public static final String MONEY =
            "   ||====================================================================||\n" +
                    "   ||//$\\\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\//$\\\\||\n" +
                    "   ||(100)==================| FEDERAL RESERVE NOTE |================(100)||\n" +
                    "   ||\\\\$//        ~         '------========--------'                \\\\$//||\n" +
                    "   ||<< /        /$\\              // ____ \\\\                         \\ >>||\n" +
                    "   ||>>|  12    //L\\\\            // ///..) \\\\         L38036133B   12 |<<||\n" +
                    "   ||<<|        \\\\ //           || <||  >\\  ||                        |>>||\n" +
                    "   ||>>|         \\$/            ||  $$ --/  ||        One Hundred     |<<||\n" +
                    "||====================================================================||>||\n" +
                    "||//$\\\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\//$\\\\||<||\n" +
                    "||(100)==================| FEDERAL RESERVE NOTE |================(100)||>||\n" +
                    "||\\\\$//        ~         '------========--------'                \\\\$//||\\||\n" +
                    "||<< /        /$\\              // ____ \\\\                         \\ >>||)||\n" +
                    "||>>|  12    //L\\\\            // ///..) \\\\         L38036133B   12 |<<||/||\n" +
                    "||<<|        \\\\ //           || <||  >\\  ||                        |>>||=||\n" +
                    "||>>|         \\$/            ||  $$ --/  ||        One Hundred     |<<||\n" +
                    "||<<|      L38036133B        *\\\\  |\\_/  //* series                 |>>||\n" +
                    "||>>|  12                     *\\\\/___\\_//*   1989                  |<<||\n" +
                    "||<<\\      Treasurer     ______/Franklin\\________     Secretary 12 />>||\n" +
                    "||//$\\                 ~|UNITED STATES OF AMERICA|~               /$\\\\||\n" +
                    "||(100)===================  ONE HUNDRED DOLLARS =================(100)||\n" +
                    "||\\\\$//\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\\\$//||\n" +
                    "||====================================================================||";
}
