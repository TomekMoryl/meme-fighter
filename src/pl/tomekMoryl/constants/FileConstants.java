package pl.tomekMoryl.constants;

public class FileConstants {
    private final static boolean isWindows = System.getProperty("os.name").toLowerCase().contains("windows");

    public final static String MAP_FILE_NAME = "worldMap";
    public final static String INTERACTION = "interaction";
    public static final String FILES_PATH = isWindows ? "files\\"
            : "files/";

}