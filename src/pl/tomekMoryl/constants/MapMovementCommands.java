package pl.tomekMoryl.constants;

public class MapMovementCommands {
    public final static String MOVE_UP = "up";
    public final static String MOVE_DOWN = "down";
    public final static String MOVE_RIGHT = "right";
    public final static String MOVE_LEFT = "left";
    public final static String SAVE = "save";
    public final static String INTERACTION = "interaction";
    public final static String ITEM = "item";
}
