package pl.tomekMoryl.constants.eventsMessages;

import pl.tomekMoryl.constants.MessageColors;

public class UpgradeMessage {
    public final static String upgrade1Image =
            "        |    |\n" +
                    "        |    o\n" +
                    "        |   /|  .--o\n" +
                    "        |   ||_/_\n" +
                    "        | .'\\    `'.\n" +
                    "        |/   \\      `\\\n" +
                    "        ||       O    |\n" +
                    "        |;            /\n" +
                    "        | \\   '-,___.'\n" +
                    "       _|  \\      \\\n" +
                    "    .-' |   ;      \\\n" +
                    "  .'    |   |       ;\n" +
                    " /      |   |       |\n" +
                    "|       |   |       ;\n" +
                    "|       L_  |      /\n" +
                    "|         `'-.  _.'\n" +
                    "\\             `'._\n" +
                    " \\                '.\n" +
                    "  '.                \\\n" +
                    "    `'._             ;\n" +
                    "        T-._         |\n" +
                    "       ,|   |-,      ;\n" +
                    "     .' |   |       /\n" +
                    "    /   |   |     .'\n" +
                    "   /    |   |.--'`\n" +
                    "  |   .-|   |\n" +
                    "  |  /  |   |\n" +
                    "  \\ /  |   |\n" +
                    "   `    |   |";


    public final static String upgrade1Message = "You found magic" + MessageColors.BRIGHT_GREEN + " centipede"
            + MessageColors.RESET + " now you can move to another area because ... 乁( ͡ಠ ʖ̯ ͡ಠ)ㄏ";


    public static String upgrade2Image =
            "         .```.   _.''..\n" +
                    "        ;     ```      ``'.\n" +
                    "        :  d               `.\n" +
                    "        / >,:                \\\n" +
                    "       /.'   `'.,             :\n" +
                    "      /'         ;.   .       ;\n" +
                    "     /          ;  \\ ;     ; /\n" +
                    "                `..;\\:     :'\n" +
                    "               __||   `...,'\n" +
                    "              `-,  )   ||\n" +
                    "               /.^/ ___||\n" +
                    "                   '---,_\\       \n" +
                    "                      (/ `\\";

    public static String upgrade2Message = MessageColors.BLUE + "Kiwi" + MessageColors.CYAN +
            "Kiwi" + MessageColors.RED + "Kiwi" + MessageColors.RESET + ", man in the valley will pass you for kiwi";


    public static String upgrade3Image =
            "                         __..-----..__\n" +
                    "                   __..--'__..-----..__`--..__\n" +
                    "            __..--'__..--'    |  ||  - `--..__`--..__\n" +
                    "           |`--..__`--..__    |  ||   __..---' __..--'\n" +
                    "            `--..__`--..__`--..__..--\\\\__..---'__..--'\n" +
                    "            |  ||  `--..__`--.._\\_..--\\)__..---' |  ||\n" +
                    "            |  ||| \\|     `--.._|_..--/|         |  ||\n" +
                    "((-.....____|  ||\\ ||        |  || \\| ||         |  ||\n" +
                    "(o \\_  .-=./|  ||`-\\|--...___|  || || /|         |  ||\n" +
                    " `.( `-.__/ |  ||  |/ -  _   |  || `--|\\-....._____ ||\n" +
                    "         / /|  ||.(/_ -    _ |  || _  ||   _       `---- .----.\n" +
                    "        / //|  ||    `--..___|  ||  _ \\\\      -   _   _ /      \\\n" +
                    "       / // |  ||___....----`|  ||__  |/_     _         |      |\n" +
                    "    ,` ` `. |  |\\\\.......--`/|  ||  `(/-....__        _ \\      /\n" +
                    "   /  \\ /  \\|  | \\\\    _   / |  ||`-.._       `---....___`.__.'\n" +
                    "   |  (O)  ||  |\\ \\\\ _    / /|  |\\\\_   `-.__ / //|  | \\\n" +
                    "   \\  / \\  /._ ||\\ \\\\    / //|  | \\\\  _     / //-|  |\\ \\.`\n" +
                    "    `/_ _\\'._ `-._\\ \\\\  / // |  |\\ \\\\          _ |  ||\\ \\\\\\\n" +
                    "             `-.__ `-._/ //  |  ||\\ \\\\  _      _      _.-'||\n" +
                    "                  `-.__ `-.__|,` ` `.`\\         _..--'_  |//\n" +
                    "                       `-.__'/  \\ /  \\\\_  _..--'   _  _.-'.\n" +
                    "                             |  (O)| || ||   _  _..--'\n" +
                    "                             \\  / \\  //_||_..--'\n" +
                    "                              `/_ _\\''   ";

    public static String upgrade3Message = "What a beautiful ram, now you can crush " + MessageColors.WHITE + "Stones\n" +
            MessageColors.RESET + "OH, and it perfectly fit in my pocket next to magic centipede and ... potatoes pestle ?";
}
