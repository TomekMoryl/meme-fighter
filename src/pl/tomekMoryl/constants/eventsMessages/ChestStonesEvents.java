package pl.tomekMoryl.constants.eventsMessages;

public class ChestStonesEvents {

    public static final String STONE_IMAGE =
            "                     /#\\\n" +
                    "                    /###\\     /\\\n" +
                    "                   /  ###\\   /##\\  /\\\n" +
                    "                  /      #\\ /####\\/##\\\n" +
                    "                 /  /      /   # /  ##\\             _       /\\\n" +
                    "               // //  /\\  /    _/  /  #\\ _         /#\\    _/##\\    /\\\n" +
                    "              // /   /  \\     /   /    #\\ \\      _/###\\_ /   ##\\__/ _\\\n" +
                    "             /  \\   / .. \\   / /   _   { \\ \\   _/       / //    /    \\\\\n" +
                    "     /\\     /    /\\  ...  \\_/   / / \\   } \\ | /  /\\  \\ /  _    /  /    \\ /\\\n" +
                    "  _ /  \\  /// / .\\  ..%:.  /... /\\ . \\ {:  \\\\   /. \\     / \\  /   ___   /  \\\n" +
                    " /.\\ .\\.\\// \\/... \\.::::..... _/..\\ ..\\:|:. .  / .. \\\\  /.. \\    /...\\ /  \\ \\\n" +
                    "/...\\.../..:.\\. ..:::::::..:..... . ...\\{:... / %... \\\\/..%. \\  /./:..\\__   \\\n" +
                    " .:..\\:..:::....:::;;;;;;::::::::.:::::.\\}.....::%.:. \\ .:::. \\/.%:::.:..\\\n" +
                    "::::...:::;;:::::;;;;;;;;;;;;;;:::::;;::{:::::::;;;:..  .:;:... ::;;::::..\n" +
                    ";;;;:::;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;];;;;;;;;;;::::::;;;;:.::;;;;;;;;:..";
    public static final String STONE_MESSAGE = "I'm not able to pass through i need something to make way";

    public static final String CHEST_IMAGE =
                    "         __________                                      \n" +
                    "        /\\____;;___\\                                     \n" +
                    "       | /         /    HERE'S A WONDERFUL TREASURE CHEST\n" +
                    "       `. ())oo() .                                      \n" +
                    "        |\\(%()*^^()^\\   I SURE HOPE IT'S NOT TRAPPED     \n" +
                    "       %| |-%-------|                                    \n" +
                    "      % \\ | %  ))   |                                    \n" +
                    "      %  \\|%________|                  ";
    public static final String CHEST_1_MESSAGE = "You found 2 potions !";
    public static final String CHEST_2_MESSAGE = "You found ... PokeBall ...";
    public static final String CHEST_3_MESSAGE = "You found fidget spinner +2 to defense";
    public static final String CHEST_4_MESSAGE = "It's the HOLY HAND GRENADE !";
    public static final String CHEST_5_MESSAGE = "You found 2 potions !";;
    public static final String CHEST_6_MESSAGE = "You found your money from holy communion, finally there they were !";
    public static final String CHEST_7_MESSAGE = "This is epic \n\n";
}

//    public static final String