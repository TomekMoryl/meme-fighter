package pl.tomekMoryl.constants;

public class MessageColors {

    public static final String BLACK = (char)27 + "[30m";
    public static final String RED = (char)27 + "[31m";
    public static final String GREEN = (char)27 + "[32m";
    public static final String YELLOW = (char)27 + "[33m";
    public static final String BLUE = (char)27 + "[34m";
    public static final String MAGENTA = (char)27 + "[35m";
    public static final String CYAN = (char)27 + "[36m";
    public static final String WHITE = (char)27 + "[37m";
    public static final String BRIGHT_BLACK = (char)27 + "[90m";
    public static final String BRIGHT_RED = (char)27 + "[91m";
    public static final String BRIGHT_GREEN = (char)27 + "[92m";
    public static final String BRIGHT_YELLOW = (char)27 + "[93m";
    public static final String BRIGHT_BLUE = (char)27 + "[94m";
    public static final String BRIGHT_MAGENTA = (char)27 + "[95m";
    public static final String BRIGHT_CYAN = (char)27 + "[96m";
    public static final String BRIGHT_WHITE = (char)27 + "[97m";
    public static final String RESET = "\033[0m";

}
