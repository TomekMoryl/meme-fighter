package pl.tomekMoryl.constants;

public class FightConstants {
    public final static String ATTACK = "attack";
    public final static String SKILL = "skill";
    public final static String ITEM = "item";
    public final static String RUN = "run";
    public static final String FIGHT_WINDOW = "   What do you want to do ?   \n" +
            MessageColors.RED + "attack  " + MessageColors.YELLOW + "skill  " + MessageColors.CYAN + "item  " +
            MessageColors.MAGENTA + "run" + MessageColors.RESET;
    public static final String VICTORY_WINDOW =
           "                                                                                   \n" +
                   "8b           d8 88   ,ad8888ba, 888888888888 ,ad8888ba,   88888888ba 8b        d8  \n" +
                   "`8b         d8' 88  d8\"'    `\"8b     88     d8\"'    `\"8b  88      \"8b Y8,    ,8P   \n" +
                   " `8b       d8'  88 d8'               88    d8'        `8b 88      ,8P  Y8,  ,8P    \n" +
                   "  `8b     d8'   88 88                88    88          88 88aaaaaa8P'   \"8aa8\"     \n" +
                   "   `8b   d8'    88 88                88    88          88 88\"\"\"\"88'      `88'      \n" +
                   "    `8b d8'     88 Y8,               88    Y8,        ,8P 88    `8b       88       \n" +
                   "     `888'      88  Y8a.    .a8P     88     Y8a.    .a8P  88     `8b      88       \n" +
                   "      `8'       88   `\"Y8888Y\"'      88      `\"Y8888Y\"'   88      `8b     88       \n" +
                   "                                                                                   ";
    public static final String LOST_WINDOW =
            "\n" +
                    "                                                                                            \n" +
                    "8b        d8 ,ad8888ba,   88        88    88          ,ad8888ba,    ad88888ba 888888888888  \n" +
                    " Y8,    ,8P d8\"'    `\"8b  88        88    88         d8\"'    `\"8b  d8\"     \"8b     88       \n" +
                    "  Y8,  ,8P d8'        `8b 88        88    88        d8'        `8b Y8,             88       \n" +
                    "   \"8aa8\"  88          88 88        88    88        88          88 `Y8aaaaa,       88       \n" +
                    "    `88'   88          88 88        88    88        88          88   `\"\"\"\"\"8b,     88       \n" +
                    "     88    Y8,        ,8P 88        88    88        Y8,        ,8P         `8b     88       \n" +
                    "     88     Y8a.    .a8P  Y8a.    .a8P    88         Y8a.    .a8P  Y8a     a8P     88       \n" +
                    "     88      `\"Y8888Y\"'    `\"Y8888Y\"'     88888888888 `\"Y8888Y\"'    \"Y88888P\"      88       \n" +
                    "                                                                                            ";
}