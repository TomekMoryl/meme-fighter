package pl.tomekMoryl.fightSystem;

import pl.tomekMoryl.constants.DisplayConstants;
import pl.tomekMoryl.constants.MessageColors;
import pl.tomekMoryl.player.Player;

import java.util.Scanner;

public class FightLogic {
    public static int attackDamage(int attackPower, int defense) {
        int damage = -1;
        while (damage < 0)
            damage = ((attackPower * (int) Math.floor(Math.random() * 10))
                    - ((int) Math.floor(Math.random() * 4) * defense));
        return damage;
    }


    public static boolean userHaveItems(Player player) {
        if (player.getUserItems().getPotions() > 0) return true;
        else if (player.getUserItems().isHolyHandGranade()) return true;
        else if (player.getUserItems().isPokeBall()) return true;
        else return player.getUserItems().isMoneyFromHolyCommunion();
    }

    public static String pickItem(Player player) {
        DisplayConstants.clearScreen();
        Scanner scanner = new Scanner(System.in);
        boolean isChoiceCorrect = false;

        while (!isChoiceCorrect) {
            System.out.println("Use items (type number of items you want to use");
            if (player.getUserItems().getPotions() > 0)
                System.out.println(MessageColors.RED + "1  Potions " + player.getUserItems().getPotions()
                        + MessageColors.RESET);
            if (player.getUserItems().isHolyHandGranade()) System.out.println(MessageColors.RED +
                    "2  Holy Hand Grenade available" + MessageColors.RESET);
            if (player.getUserItems().isPokeBall()) System.out.println(MessageColors.RED +
                    "3  Pokeball available" + MessageColors.RESET);
            if (player.getUserItems().isMoneyFromHolyCommunion())
                System.out.println(MessageColors.RED + "4  Money From Holy Communion available, but don't use it"
                        + MessageColors.RESET);
            System.out.println(MessageColors.MAGENTA + "    If you want to go back type 'back'\n" + MessageColors.RESET);

            switch (scanner.nextLine()) {
                case ("1"): {
                    if (player.getUserItems().getPotions() > 0) {
                        player.getUserItems().setPotions(player.getUserItems().getPotions() - 1);
                        return "potion";
                    } else {
                        System.out.println("No potions");
                        DisplayConstants.waitForEnter();
                        break;
                    }
                }
                case ("2"): {
                    if (player.getUserItems().isHolyHandGranade()) {
                        player.getUserItems().setHolyHandGranade(false);
                        return "grenade";
                    } else {
                        System.out.println("No Holy Hand Grenade");
                        DisplayConstants.waitForEnter();
                        break;
                    }
                }
                case ("3"): {
                    if (player.getUserItems().isPokeBall()) {
                        player.getUserItems().setPokeBall(false);
                        return "pokeball";
                    } else {
                        System.out.println("No PokeBall");
                        DisplayConstants.waitForEnter();
                        break;
                    }
                }
                case ("4"): {
                    if (player.getUserItems().isMoneyFromHolyCommunion()) {
                        player.getUserItems().setMoneyFromHolyCommunion(false);
                        return "money";
                    } else {
                        System.out.println("No Money :(");
                        DisplayConstants.waitForEnter();
                        break;
                    }
                }
                case ("back"): {
                    return "back";
                }
                default: {
                    System.out.println("  Something wrong with this command try again ");
                    DisplayConstants.waitForEnter();
                    break;
                }
            }
        }
        return "back";
    }
}


