package pl.tomekMoryl.fightSystem;

import pl.tomekMoryl.constants.*;
import pl.tomekMoryl.constants.hero.WeaponsConstants;
import pl.tomekMoryl.gameLogic.Game;
import pl.tomekMoryl.player.Player;
import pl.tomekMoryl.player.PlayerLogic;

import java.util.Objects;
import java.util.Scanner;

public class Fight {
    private Player player;
    private int enemyHealth;
    private int enemyAttack;
    private int enemyDefense;
    private String enemyImage;
    private int experienceReward;

    public Fight() {
    }

    public Fight(Player player, int enemyHealth, int enemyAttack, int enemyDefense, String enemyImage, int experienceReward) {
        this.player = player;
        this.enemyHealth = enemyHealth;
        this.enemyAttack = enemyAttack;
        this.enemyDefense = enemyDefense;
        this.enemyImage = enemyImage;
        this.experienceReward = experienceReward;
    }

    public void startFight() {
        DisplayConstants.fightIntro();
        int heroHP = player.getHP();
        boolean isFightInProgress = true;
        while (isFightInProgress) {
            Scanner scanner = new Scanner(System.in);
            boolean isCommandCorrect = false;
            while (!isCommandCorrect) {
                System.out.println(enemyImage + "\n");
                System.out.println(FightConstants.FIGHT_WINDOW);
                System.out.println("Hero HP " + MessageColors.RED + heroHP + "" + MessageColors.RESET);
                System.out.println("Enemy HP " + MessageColors.RED + enemyHealth + "" + MessageColors.RESET);
                String inputCommand = scanner.nextLine();
                switch (inputCommand) {
                    case (FightConstants.ATTACK): {
                        int damage = FightLogic.attackDamage(player.getAttackPower(), enemyDefense);
                        enemyHealth = enemyHealth - damage;
                        System.out.println("You inflicted " + MessageColors.RED + damage + "" + MessageColors.RESET);
                        isCommandCorrect = true;
                        break;
                    }
                    case (FightConstants.SKILL): {
                        int damage = FightLogic.attackDamage(player.getSkillsPower(), enemyDefense);
                        enemyHealth = enemyHealth - damage;
                        System.out.println("You inflicted " + MessageColors.RED + damage + "" + MessageColors.RESET);
                        isCommandCorrect = true;
                        break;
                    }
                    case (FightConstants.ITEM): {
                        boolean isItemAvailable = FightLogic.userHaveItems(player);
                        while (isItemAvailable) {
                            String itemToUse = FightLogic.pickItem(player);
                            switch (itemToUse) {
                                case ("potion"): {
                                    System.out.println("You used potion now, you are fully healed");
                                    DisplayConstants.waitForEnter();
                                    heroHP = player.getHP();
                                    isItemAvailable = false;
                                    isCommandCorrect = true;
                                    break;
                                }
                                case ("grenade"): {
                                    System.out.println("You used holy hand grenade, and you killed enemy");
                                    DisplayConstants.waitForEnter();
                                    enemyHealth = -1;
                                    isItemAvailable = false;
                                    isCommandCorrect = true;
                                    break;
                                }
                                case ("pokeball"): {
                                    System.out.println("You used PokeBall,");
                                    DisplayConstants.waitForEnter();
                                    System.out.println(WeaponsConstants.POKEBALL);
                                    System.out.println("\n    ...Nothing happend, what were you thinking ? ");
                                    DisplayConstants.waitForEnter();
                                    isItemAvailable = false;
                                    isCommandCorrect = true;
                                    break;
                                }
                                case ("money"): {
                                    System.out.println(WeaponsConstants.MONEY);
                                    System.out.println("\n    Look closely, you lost them ... again ");
                                    DisplayConstants.waitForEnter();
                                    System.out.println("\n    Such a Great idea ... ");
                                    DisplayConstants.waitForEnter();
                                    System.out.println("\n    Just tell me why you use money on enemy ?? ");
                                    DisplayConstants.waitForEnter();
                                    isItemAvailable = false;
                                    isCommandCorrect = true;
                                    break;
                                }
                                case ("back"): {
                                    isItemAvailable = false;
                                    isCommandCorrect = false;
                                    break;
                                }
                                default: {
                                    System.out.println("Something, Wrong try again");
                                }
                            }
                        }
                        break;
                    }

                    case (FightConstants.RUN): {
                        System.out.println("don't you even try typing that again it's not working \n " +
                                " (∩ ͡ ° ʖ ͡ °) ⊃-(===>   (YOU)   <:::::[]=¤ (▀̿̿Ĺ̯̿̿▀̿ ̿),");
                        DisplayConstants.waitForEnter();
                        break;
                    }
                    default: {
                        System.out.println(" \n Invalid command try again  \n");
                        DisplayConstants.waitForEnter();
                        break;
                    }
                }
            }
            if (enemyHealth < 1) {
                System.out.println(FightConstants.VICTORY_WINDOW);
                System.out.println("  You earned " + experienceReward + "EXP");
                DisplayConstants.waitForEnter();
                PlayerLogic.levelUP(player, experienceReward);
                isFightInProgress = false;
            } else {
                int damage = FightLogic.attackDamage(enemyAttack, player.getDefense());
                System.out.println("You received " + MessageColors.RED + damage + MessageColors.RESET);
                heroHP = heroHP - damage;
                if (heroHP < 1) {
                    System.out.println(FightConstants.LOST_WINDOW);
                    System.out.println("    Try Again from beginning");
                    DisplayConstants.waitForEnter();
                    isFightInProgress = false;
                    Game.gameEnd = true;
                }
            }
            DisplayConstants.waitForEnter();
        }
    }


    public void randomFight(Player player) {
        new Fight(player, (int) (player.getHP() * 0.4), player.getLevel() +1,
                player.getLevel() + 1, MinionsConstants.randomEnemy(),
                (player.getLevel() * 15)).startFight();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int getEnemyHealth() {
        return enemyHealth;
    }

    public void setEnemyHealth(int enemyHealth) {
        this.enemyHealth = enemyHealth;
    }

    public int getEnemyAttack() {
        return enemyAttack;
    }

    public void setEnemyAttack(int enemyAttack) {
        this.enemyAttack = enemyAttack;
    }

    public int getEnemyDefense() {
        return enemyDefense;
    }

    public void setEnemyDefense(int enemyDefense) {
        this.enemyDefense = enemyDefense;
    }

    public String getEnemyImage() {
        return enemyImage;
    }

    public void setEnemyImage(String enemyImage) {
        this.enemyImage = enemyImage;
    }

    public int getExperienceReward() {
        return experienceReward;
    }

    public void setExperienceReward(int experienceReward) {
        this.experienceReward = experienceReward;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Fight)) return false;
        Fight fight = (Fight) o;
        return enemyHealth == fight.enemyHealth &&
                enemyAttack == fight.enemyAttack &&
                enemyDefense == fight.enemyDefense &&
                experienceReward == fight.experienceReward &&
                Objects.equals(player, fight.player) &&
                Objects.equals(enemyImage, fight.enemyImage);
    }

    @Override
    public int hashCode() {

        return Objects.hash(player, enemyHealth, enemyAttack, enemyDefense, enemyImage, experienceReward);
    }

    @Override
    public String toString() {
        return "Fight{" +
                "player=" + player +
                ", enemyHealth=" + enemyHealth +
                ", enemyAttack=" + enemyAttack +
                ", enemyDefense=" + enemyDefense +
                ", enemyImage='" + enemyImage + '\'' +
                ", experienceReward=" + experienceReward +
                '}';
    }
}
