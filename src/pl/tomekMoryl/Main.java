package pl.tomekMoryl;

import pl.tomekMoryl.gameLogic.Game;

public class Main {


    public static void main(String[] args) {
        Game game = new Game();
        game.startGame();
    }
}
