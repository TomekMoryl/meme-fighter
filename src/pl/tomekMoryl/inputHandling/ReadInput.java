package pl.tomekMoryl.inputHandling;

import java.util.Scanner;

public class ReadInput {

    public static String readKeyboardButton() {
        Scanner keyboard = new Scanner(System.in);
        return keyboard.nextLine();
    }

}
