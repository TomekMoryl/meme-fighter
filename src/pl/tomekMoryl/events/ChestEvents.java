package pl.tomekMoryl.events;

import pl.tomekMoryl.constants.DisplayConstants;
import pl.tomekMoryl.constants.HeroConstants;
import pl.tomekMoryl.constants.eventsMessages.ChestStonesEvents;
import pl.tomekMoryl.constants.hero.WeaponsConstants;
import pl.tomekMoryl.gameMap.logic.MapMovement;
import pl.tomekMoryl.player.Player;
import pl.tomekMoryl.player.PlayerPosition;

import java.util.Arrays;
import java.util.List;

public class ChestEvents {

    private int[][] chest1 = new int[][]{{16, 2}};
    private int[][] chest2 = new int[][]{{15, 18}};
    private int[][] chest3 = new int[][]{{20, 30}};
    private int[][] chest4 = new int[][]{{30, 32}};
    private int[][] chest5 = new int[][]{{23, 44}};
    private int[][] chest6 = new int[][]{{30, 56}};
    private int[][] chest7 = new int[][]{{8, 24}};

    public void handleChestEvent(Player player, PlayerPosition playerPosition) {

        List<int[][]> chestList = Arrays.asList(chest1, chest2, chest3, chest4, chest5, chest6, chest7);
        int i = 0;
        String chest = "";

        for (int[][] findNpc : chestList) {
            i++;
            if (Arrays.deepEquals(findNpc, new int[][]{{playerPosition.getX(), playerPosition.getY()}}))
                chest = "chest" + i;
        }
        switch (chest) {
            case ("chest1"): {
                System.out.println(ChestStonesEvents.CHEST_IMAGE + "\n ");
                System.out.println(ChestStonesEvents.CHEST_1_MESSAGE + "\n \n");
                MapMovement.previousField = '#';
                player.getUserItems().setPotions(player.getUserItems().getPotions() + 2);
                DisplayConstants.waitForEnter();
                break;
            }
            case ("chest2"): {
                System.out.println(ChestStonesEvents.CHEST_IMAGE + "\n ");
                System.out.println(ChestStonesEvents.CHEST_2_MESSAGE + "\n \n");
                MapMovement.previousField = '*';
                player.getUserItems().setPokeBall(true);
                DisplayConstants.waitForEnter();
                break;
            }
            case ("chest3"): {
                System.out.println(ChestStonesEvents.CHEST_IMAGE + "\n ");
                System.out.println(ChestStonesEvents.CHEST_3_MESSAGE + "\n \n");
                MapMovement.previousField = '*';
                player.setDefense(player.getDefense() + 2);
                DisplayConstants.waitForEnter();
                break;
            }
            case ("chest4"): {
                System.out.println(ChestStonesEvents.CHEST_IMAGE + "\n ");
                System.out.println(ChestStonesEvents.CHEST_4_MESSAGE + "\n \n");
                MapMovement.previousField = '#';
                player.getUserItems().setHolyHandGranade(true);
                DisplayConstants.waitForEnter();
                break;
            }
            case ("chest5"): {
                System.out.println(ChestStonesEvents.CHEST_IMAGE + "\n ");
                System.out.println(ChestStonesEvents.CHEST_5_MESSAGE + "\n \n");
                MapMovement.previousField = '#';
                player.getUserItems().setPotions(player.getUserItems().getPotions() + 2);
                DisplayConstants.waitForEnter();
                break;
            }
            case ("chest6"): {
                System.out.println(ChestStonesEvents.CHEST_IMAGE + "\n ");
                System.out.println(ChestStonesEvents.CHEST_6_MESSAGE + "\n \n");
                MapMovement.previousField = '#';
                player.getUserItems().setMoneyFromHolyCommunion(true);
                DisplayConstants.waitForEnter();
                break;
            }
            case ("chest7"): {
                System.out.println(ChestStonesEvents.CHEST_IMAGE + "\n ");
                System.out.println(ChestStonesEvents.CHEST_7_MESSAGE + "\n \n");
                MapMovement.previousField = '#';
                DisplayConstants.waitForEnter();

                switch (player.getHeroClass()) {
                    case (HeroConstants.KNIGHT): {
                        player.getUserItems().getWeapons().add(WeaponsConstants.CHA_CHING_SWORD);
                        System.out.println(WeaponsConstants.CHA_CHING_SWORD_IMAGE + "\n ");
                        System.out.println("             You found " + WeaponsConstants.CHA_CHING_SWORD + "\n\n");
                        System.out.println("You are automatically using it, +4 to attack power");
                        player.setEquippedWeapon(WeaponsConstants.CHA_CHING_SWORD);
                        player.setAttackPower(player.getAttackPower()+1);
                        DisplayConstants.waitForEnter();
                        break;
                    }
                    case (HeroConstants.WARRIOR): {
                        player.getUserItems().getWeapons().add(WeaponsConstants.SHUSH_AXE);
                        System.out.println(WeaponsConstants.SHUSH_AXE_IMAGE + "\n ");
                        System.out.println("            You found " + WeaponsConstants.SHUSH_AXE + "\n\n");
                        System.out.println("You are automatically using it, +4 to attack power");
                        player.setEquippedWeapon(WeaponsConstants.SHUSH_AXE);
                        player.setAttackPower(player.getAttackPower()+1);
                        DisplayConstants.waitForEnter();
                        break;
                    }
                    case (HeroConstants.MAGE): {
                        player.getUserItems().getWeapons().add(WeaponsConstants.PEW_PEW_STAFF);
                        System.out.println(WeaponsConstants.PEW_PEW_STAFF_IMAGE + "\n ");
                        System.out.println("          You found " + WeaponsConstants.PEW_PEW_STAFF + "\n\n");
                        System.out.println("You are automatically using it, +4 to attack power");
                        player.setEquippedWeapon(WeaponsConstants.PEW_PEW_STAFF);
                        player.setAttackPower(player.getAttackPower()+1);
                        DisplayConstants.waitForEnter();
                        break;
                    }
                }
                DisplayConstants.waitForEnter();
                break;
            }
        }
    }
}
