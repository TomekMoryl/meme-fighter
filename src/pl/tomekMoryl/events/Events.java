package pl.tomekMoryl.events;

import pl.tomekMoryl.player.Player;
import pl.tomekMoryl.player.PlayerPosition;

public class Events {
    private NPCEvents npcEvents = new NPCEvents();
    private UpgradeEvents upgradeEvents = new UpgradeEvents();
    private BlockPathEvent blockPathEvent = new BlockPathEvent();
    private ChestEvents chestEvents = new ChestEvents();
    private EnemyEvents enemyEvents = new EnemyEvents();

    public void eventLoader(char field, Player player, PlayerPosition playerPosition) {

        switch (field) {
            case ('N'): {
                npcEvents.handleNPCEvent(player, playerPosition);
                break;
            }
            case ('U'): {
                upgradeEvents.handleUpgradeEvent(player, playerPosition);
                break;
            }
            case ('C'): {
                chestEvents.handleChestEvent(player, playerPosition);
                break;
            }
            case ('S'): {
                blockPathEvent.stonesDestroy(player, playerPosition);
                break;
            }
            case ('E'):
            case ('@'): {
                enemyEvents.handleEnemyEvent(player, playerPosition);
                break;
            }

            default: {
                System.out.print("\n\n Where are everybody ? \n\n");
            }
        }
    }

}
