package pl.tomekMoryl.events;

import pl.tomekMoryl.constants.DisplayConstants;
import pl.tomekMoryl.constants.HeroConstants;
import pl.tomekMoryl.constants.eventsMessages.npc.NPCDialogs;
import pl.tomekMoryl.constants.hero.WeaponsConstants;
import pl.tomekMoryl.gameMap.logic.MapMovement;
import pl.tomekMoryl.player.Player;
import pl.tomekMoryl.player.PlayerPosition;

import java.util.Arrays;
import java.util.List;

class NPCEvents {

    private int[][] npc1 = new int[][]{{2, 14}};
    private int[][] npc2 = new int[][]{{2, 20}};
    private int[][] npc3 = new int[][]{{7, 10}};
    private int[][] npc4 = new int[][]{{21, 8}};
    private int[][] npc5 = new int[][]{{20, 20}};
    private int[][] npc6 = new int[][]{{25, 26}};
    private int[][] npc7 = new int[][]{{29, 14}};
    private int[][] npc8 = new int[][]{{25, 60}};
    private int[][] npc9 = new int[][]{{4, 52}};

    void handleNPCEvent(Player player, PlayerPosition playerPosition) {
        List<int[][]> npcList = Arrays.asList(npc1, npc2, npc3, npc4, npc5, npc6, npc7, npc8, npc9);
        int i = 0;
        String npc = "";

        for (int[][] findNpc : npcList) {
            i++;
            if (Arrays.deepEquals(findNpc, new int[][]{{playerPosition.getX(), playerPosition.getY()}}))
                npc = "npc" + i;
        }
        switch (npc) {
            case ("npc1"): {
                System.out.println(NPCDialogs.npc1Image + "\n ");
                System.out.println(NPCDialogs.npc1Dialog + "\n \n");
                MapMovement.previousField = '.';
                DisplayConstants.waitForEnter();
                break;
            }
            case ("npc2"): {
                System.out.println(NPCDialogs.npc2Image + "\n ");
                System.out.println(NPCDialogs.npc2Dialog + "\n \n");
                MapMovement.previousField = '#';
                DisplayConstants.waitForEnter();
                break;
            }
            case ("npc3"): {
                System.out.println(NPCDialogs.npc3Image + "\n ");
                System.out.println(NPCDialogs.npc3DialogSuccess + "\n \n");
                MapMovement.previousField = '#';
                DisplayConstants.waitForEnter();
                switch (player.getHeroClass()) {
                    case (HeroConstants.KNIGHT):
                        System.out.println(WeaponsConstants.SMALL_SWORD_IMAGE + "\n ");
                        System.out.println(WeaponsConstants.SMALL_SWORD_DIALOG + "\n \n");
                        player.getUserItems().getWeapons().add(WeaponsConstants.SMALL_SWORD);
                        System.out.println("You are automatically using it, +1 to attack power");
                        player.setEquippedWeapon(WeaponsConstants.SMALL_SWORD);
                        player.setAttackPower(player.getAttackPower()+1);
                        DisplayConstants.waitForEnter();
                        break;
                    case (HeroConstants.MAGE):
                        System.out.println(WeaponsConstants.SMALL_STAFF_IMAGE + "\n ");
                        System.out.println(WeaponsConstants.SMALL_STAFF_DIALOG + "\n \n");
                        player.getUserItems().getWeapons().add(WeaponsConstants.SMALL_STAFF);
                        System.out.println("You are automatically using it, +1 to attack power");
                        player.setEquippedWeapon(WeaponsConstants.SMALL_STAFF);
                        player.setAttackPower(player.getAttackPower()+1);
                        DisplayConstants.waitForEnter();
                        break;
                    case (HeroConstants.WARRIOR):
                        System.out.println(WeaponsConstants.SMALL_AXE_IMAGE + "\n ");
                        System.out.println(WeaponsConstants.SMALL_AXE_DIALOG + "\n \n");
                        player.getUserItems().getWeapons().add(WeaponsConstants.SMALL_AXE);
                        System.out.println("You are automatically using it, +1 to attack power");
                        player.setEquippedWeapon(WeaponsConstants.SMALL_AXE);
                        player.setAttackPower(player.getAttackPower()+1);
                        DisplayConstants.waitForEnter();
                        break;
                }
                player.setTutorialFinished(true);
                break;
            }
            case ("npc4"): {
                System.out.println(NPCDialogs.npc4Image + "\n ");
                System.out.println(NPCDialogs.npc4Dialog + "\n \n");
                MapMovement.previousField = '*';
                DisplayConstants.waitForEnter();
                break;
            }
            case ("npc5"): {
                System.out.println(NPCDialogs.npc5Image + "\n ");
                System.out.println(NPCDialogs.npc5Dialog + "\n \n");
                MapMovement.previousField = '*';
                DisplayConstants.waitForEnter();
                break;
            }
            case ("npc6"): {
                System.out.println(NPCDialogs.npc6Image + "\n ");
                System.out.println(NPCDialogs.npc6DialogSuccess + "\n \n");
                MapMovement.previousField = '.';
                DisplayConstants.waitForEnter();
                switch (player.getHeroClass()) {
                    case (HeroConstants.KNIGHT):
                        System.out.println(WeaponsConstants.MEDIUM_SWORD_IMAGE + "\n ");
                        System.out.println(WeaponsConstants.MEDIUM_SWORD_DIALOG + "\n \n");
                        System.out.println("You are automatically using it, +2 to attack power");
                        player.setEquippedWeapon(WeaponsConstants.MEDIUM_SWORD);
                        player.setAttackPower(player.getAttackPower()+1);
                        DisplayConstants.waitForEnter();
                        break;
                    case (HeroConstants.MAGE):
                        System.out.println(WeaponsConstants.MEDIUM_STAFF_IMAGE + "\n ");
                        System.out.println(WeaponsConstants.MEDIUM_STAFF_DIALOG + "\n \n");
                        System.out.println("You are automatically using it, +2 to attack power");
                        player.setEquippedWeapon(WeaponsConstants.MEDIUM_STAFF);
                        player.setAttackPower(player.getAttackPower()+1);
                        DisplayConstants.waitForEnter();
                        break;
                    case (HeroConstants.WARRIOR):
                        System.out.println(WeaponsConstants.MEDIUM_AXE_IMAGE + "\n ");
                        System.out.println(WeaponsConstants.MEDIUM_AXE_DIALOG + "\n \n");
                        System.out.println("You are automatically using it, +2 to attack power");
                        player.setEquippedWeapon(WeaponsConstants.MEDIUM_AXE);
                        player.setAttackPower(player.getAttackPower()+1);
                        DisplayConstants.waitForEnter();
                        break;
                }
                break;
            }
            case ("npc7"): {
                System.out.println(NPCDialogs.npc7Image + "\n ");
                System.out.println(NPCDialogs.npc7Dialog + "\n \n");
                MapMovement.previousField = '.';
                DisplayConstants.waitForEnter();
                break;
            }
            case ("npc8"): {
                System.out.println(NPCDialogs.npc8Image + "\n ");
                System.out.println(NPCDialogs.npc8Dialog + "\n \n");
                MapMovement.previousField = '#';
                DisplayConstants.waitForEnter();
                break;
            }
            case ("npc9"): {
                System.out.println(NPCDialogs.npc9Image + "\n ");
                System.out.println(NPCDialogs.npc9Dialog + "\n \n");
                MapMovement.previousField = '*';
                DisplayConstants.waitForEnter();
                break;
            }
        }
    }
}
