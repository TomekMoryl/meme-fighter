package pl.tomekMoryl.events;

import pl.tomekMoryl.constants.DisplayConstants;
import pl.tomekMoryl.constants.HeroConstants;
import pl.tomekMoryl.constants.eventsMessages.ChestStonesEvents;
import pl.tomekMoryl.constants.eventsMessages.npc.NPCDialogs;
import pl.tomekMoryl.constants.hero.UpgradesConstants;
import pl.tomekMoryl.constants.hero.WeaponsConstants;
import pl.tomekMoryl.gameMap.logic.MapMovement;
import pl.tomekMoryl.player.Player;
import pl.tomekMoryl.player.PlayerPosition;

import java.util.Arrays;
import java.util.List;

public class BlockPathEvent {

    public boolean isMoveBlocked(char field, Player player, int xAxis, int yAxis) {
        int[][] blocker1 = new int[][]{{7, 10}};
        int[][] blocker2 = new int[][]{{25, 26}};
        int[][] blocker3 = new int[][]{{14, 48}};
        int[][] blocker4 = new int[][]{{6, 28}};

        List<int[][]> npcList = Arrays.asList(blocker1, blocker2, blocker3, blocker4);
        int i = 0;
        String blocker = "";

        for (int[][] findNpc : npcList) {
            i++;
            if (Arrays.deepEquals(findNpc, new int[][]{{xAxis, yAxis}}))
                blocker = "blocker" + i;
        }
        switch (blocker) {
            case ("blocker1"): {
                if (player.getUserItems().getHeroUpgrades().contains(UpgradesConstants.UPGRADE1))
                    return true;
                System.out.println(NPCDialogs.nbc3ImageFailure + "\n ");
                System.out.println(NPCDialogs.npc3DialogFailure + "\n \n");
                DisplayConstants.waitForEnter();
                return false;
            }
            case ("blocker2"): {
                if (player.getUserItems().getHeroUpgrades().contains(UpgradesConstants.UPGRADE2))
                    return true;
                System.out.println(NPCDialogs.npc6Image + "\n ");
                System.out.println(NPCDialogs.npc6DialogFailure + "\n \n");
                DisplayConstants.waitForEnter();
                return false;
            }
            case ("blocker3"):
            case ("blocker4"): {
                if (player.getUserItems().getHeroUpgrades().contains(UpgradesConstants.UPGRADE3))
                    return true;
                System.out.println(ChestStonesEvents.STONE_IMAGE + "\n ");
                System.out.println(ChestStonesEvents.STONE_MESSAGE + "\n \n");
                DisplayConstants.waitForEnter();
                return false;
            }

            default:
        }
        return true;
    }

    public void stonesDestroy(Player player, PlayerPosition playerPosition) {

        int[][] blocker1 = new int[][]{{14, 48}};
        int[][] blocker2 = new int[][]{{6, 28}};

        List<int[][]> npcList = Arrays.asList(blocker1, blocker2);
        int i = 0;
        String blocker = "";

        for (int[][] findNpc : npcList) {
            i++;
            if (Arrays.deepEquals(findNpc, new int[][]{{playerPosition.getX(), playerPosition.getY()}}))
                blocker = "blocker" + i;
        }

        switch (blocker) {
            case ("blocker1"): {
                System.out.println("Now there is path \n");
                System.out.println("I found something");
                MapMovement.previousField = '.';
                DisplayConstants.waitForEnter();
                switch (player.getHeroClass()) {
                    case (HeroConstants.KNIGHT):
                        System.out.println(WeaponsConstants.BIG_SWORD_IMAGE + "\n ");
                        System.out.println(WeaponsConstants.BIG_SWORD_DIALOG + "\n \n");
                        System.out.println("You are automatically using it, +3 to attack power");
                        player.setEquippedWeapon(WeaponsConstants.BIG_SWORD);
                        player.setAttackPower(player.getAttackPower()+1);
                        DisplayConstants.waitForEnter();
                        break;
                    case (HeroConstants.MAGE):
                        System.out.println(WeaponsConstants.BIG_STAFF_IMAGE + "\n ");
                        System.out.println(WeaponsConstants.BIG_STAFF_DIALOG + "\n \n");
                        System.out.println("You are automatically using it, +3 to attack power");
                        player.setEquippedWeapon(WeaponsConstants.BIG_STAFF);
                        player.setAttackPower(player.getAttackPower()+1);
                        DisplayConstants.waitForEnter();
                        break;
                    case (HeroConstants.WARRIOR):
                        System.out.println(WeaponsConstants.BIG_AXE_IMAGE + "\n ");
                        System.out.println(WeaponsConstants.BIG_AXE_DIALOG + "\n \n");
                        System.out.println("You are automatically using it, +3 to attack power");
                        player.setEquippedWeapon(WeaponsConstants.BIG_AXE);
                        player.setAttackPower(player.getAttackPower()+1);
                        DisplayConstants.waitForEnter();
                        break;
                }
                break;
            }
            case ("blocker2"): {
                System.out.println("Now there is path \n \n");
                MapMovement.previousField = '#';
                DisplayConstants.waitForEnter();
                break;
            }
        }
    }
}
