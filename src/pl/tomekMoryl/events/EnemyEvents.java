package pl.tomekMoryl.events;

import pl.tomekMoryl.constants.DisplayConstants;
import pl.tomekMoryl.constants.EnemyConstants;
import pl.tomekMoryl.fightSystem.Fight;
import pl.tomekMoryl.gameLogic.Game;
import pl.tomekMoryl.gameMap.logic.MapMovement;
import pl.tomekMoryl.player.Player;
import pl.tomekMoryl.player.PlayerPosition;

import java.util.Arrays;
import java.util.List;

public class EnemyEvents {

    private int[][] enemy1 = new int[][]{{18, 20}};
    private int[][] enemy2 = new int[][]{{28, 2}};
    private int[][] enemy3 = new int[][]{{29, 46}};
    private int[][] enemy4 = new int[][]{{6, 24}};
    private int[][] enemy5 = new int[][]{{4, 58}};
    private int[][] enemy6 = new int[][]{{1, 56}};
    private int[][] enemy7 = new int[][]{{2, 52}};

    public void handleEnemyEvent(Player player, PlayerPosition playerPosition) {

        List<int[][]> enemyList = Arrays.asList(enemy1, enemy2, enemy3, enemy4, enemy5, enemy6, enemy7);
        int i = 0;
        String enemy = "";

        for (int[][] findEnemy : enemyList) {
            i++;
            if (Arrays.deepEquals(findEnemy, new int[][]{{playerPosition.getX(), playerPosition.getY()}}))
                enemy = "enemy" + i;
        }

        switch (enemy) {
            case ("enemy1"): {
                new Fight(player, 50, player.getLevel(), 0,
                        EnemyConstants.ENEMY_1_IMAGE, 50).startFight();
                MapMovement.previousField = '*';
                break;
            }
            case ("enemy2"): {
                new Fight(player, 100, player.getLevel() + 1, 1,
                        EnemyConstants.ENEMY_2_IMAGE, 100).startFight();
                MapMovement.previousField = '#';
                break;
            }
            case ("enemy3"): {
                new Fight(player, 150, player.getLevel() + 6, 2,
                        EnemyConstants.ENEMY_3_IMAGE, 150).startFight();
                MapMovement.previousField = '*';
                break;
            }
            case ("enemy4"): {
                new Fight(player, 250, player.getLevel() + 3, 3,
                        EnemyConstants.ENEMY_4_IMAGE, 300).startFight();
                MapMovement.previousField = '#';
                break;
            }
            case ("enemy5"): {
                new Fight(player, 280, player.getLevel() + 3, 4,
                        EnemyConstants.ENEMY_5_IMAGE, 350).startFight();
                MapMovement.previousField = '*';
                break;
            }
            case ("enemy6"): {
                new Fight(player, 350, player.getLevel() + 2, 5,
                        EnemyConstants.ENEMY_6_IMAGE, 400).startFight();
                MapMovement.previousField = '*';
                break;
            }
            case ("enemy7"): {
                new Fight(player, 340, player.getLevel() + 2, 7,
                        EnemyConstants.ENEMY_7_IMAGE, 500).startFight();
                if (!Game.gameEnd) DisplayConstants.gameFinal();
                Game.gameEnd = true;
                break;
            }
        }
    }
}

