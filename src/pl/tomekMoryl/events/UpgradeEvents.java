package pl.tomekMoryl.events;

import pl.tomekMoryl.constants.DisplayConstants;
import pl.tomekMoryl.constants.eventsMessages.UpgradeMessage;
import pl.tomekMoryl.constants.hero.UpgradesConstants;
import pl.tomekMoryl.gameMap.logic.MapMovement;
import pl.tomekMoryl.player.Player;
import pl.tomekMoryl.player.PlayerPosition;

import java.util.Arrays;
import java.util.List;

public class UpgradeEvents {
    private int[][] upgrade1 = new int[][]{{3, 12}};
    private int[][] upgrade2 = new int[][]{{30, 4}};
    private int[][] upgrade3 = new int[][]{{15, 32}};

    void handleUpgradeEvent(Player player, PlayerPosition playerPosition) {
        List<int[][]> npcList = Arrays.asList(upgrade1, upgrade2, upgrade3);
        int i = 0;
        String npc = "";

        for (int[][] findNpc : npcList) {
            i++;
            if (Arrays.deepEquals(findNpc, new int[][]{{playerPosition.getX(), playerPosition.getY()}}))
                npc = "upgrade" + i;
        }
        switch (npc) {
            case ("upgrade1"): {
                System.out.println(UpgradeMessage.upgrade1Image + "\n ");
                System.out.println(UpgradeMessage.upgrade1Message + "\n \n");
                MapMovement.previousField = '.';
                player.getUserItems().getHeroUpgrades().add(UpgradesConstants.UPGRADE1);
                DisplayConstants.waitForEnter();
                break;
            }
            case ("upgrade2"): {
                System.out.println(UpgradeMessage.upgrade2Image + "\n ");
                System.out.println(UpgradeMessage.upgrade2Message + "\n \n");
                MapMovement.previousField = '*';
                player.getUserItems().getHeroUpgrades().add(UpgradesConstants.UPGRADE2);
                DisplayConstants.waitForEnter();
                break;
            }
            case ("upgrade3"): {
                System.out.println(UpgradeMessage.upgrade3Image + "\n ");
                System.out.println(UpgradeMessage.upgrade3Message + "\n \n");
                MapMovement.previousField = '#';
                player.getUserItems().getHeroUpgrades().add(UpgradesConstants.UPGRADE3);
                DisplayConstants.waitForEnter();
                break;
            }
        }
    }
}
